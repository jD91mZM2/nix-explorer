with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "nix-explorer";
  buildInputs = [ ncurses ];
}
